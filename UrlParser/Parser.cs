using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace UrlParser
{
    public static class Parser
    {
        static string hrefPattern = @"\b(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)";
        public static IEnumerable<HashSet<string>> ParseUrl( string html)
        {
            string htmldata = GetHtmlPageText(html);
            Regex regex = new Regex( hrefPattern);
            MatchCollection matches = regex.Matches( htmldata );
            HashSet<string> UniqCollection = new HashSet<string>();
            foreach (Match match in matches)
            {
                UniqCollection.Add(match.Value);
                yield return UniqCollection;
            }
        }

        public static string GetHtmlPageText(string url)
        {
            WebClient client = new WebClient();
            using (Stream data = client.OpenRead(url))
            {
                using (StreamReader reader = new StreamReader(data))
                {
                    return reader.ReadToEnd();
                }
            }
        }
        
    }
    
}