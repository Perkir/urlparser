﻿using System;
using System.Linq;

namespace UrlParser
{
    /// <summary>
    /// 1. На вход принимает URL-адрес,
    /// 2. Загружает HTML-текст с этого адреса.
    /// 3. Находит URL-адреса, используя регулярное выражение.
    /// 4. Выводит на экран список всех найденных URL-адресов.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите URL:");
            var readLine = Console.ReadLine();
            string html = (readLine == null) ? "http://google.ru": readLine;
            int increment = 0;
            foreach(var hashSetsUrls in Parser.ParseUrl(html))
            {
                
                foreach (var urls in hashSetsUrls)
                {
                    increment += 1;
                    Console.WriteLine($"{increment}: {urls}");
                }
                
            }

            Console.ReadKey();
        }

        
    }
}